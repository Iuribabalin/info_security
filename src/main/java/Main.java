import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Main {
    private static Map<String, String> symbolMapUp = new HashMap<>();
    private static List<Character> rusUp = new ArrayList<>();

    public static void main(String[] args) {
        initMapRu();
        Scanner in = new Scanner(System.in);
        System.out.print("Введите ключ key: ");
        String key = in.nextLine();
        System.out.print("Введите путь к файлу path: ");
        String path = in.nextLine();
        System.out.print("Введите сдвиг shift: ");
        int shift = in.nextInt();
        //src/main/resources/test.txt
        String text = FileInput.getTextFile(path);
        Cezar.initMap(shift, key.toUpperCase(), symbolMapUp, rusUp);
        String code = Cezar.toCode(text);
        String deCode = Cezar.deCode(code);
        System.out.println("Сдвиг: " + shift);
        System.out.println("Ключ: " + key);
        System.out.println("Изначальный текст: " + text);
        System.out.println("Закодированый: " + code + "\nДекодированый: " + deCode);
    }


    private static void initMapRu() {
        for (char i = 'А'; i <= 'Я'; i++) {
            rusUp.add(i);
            symbolMapUp.put(String.valueOf(i), "");
        }
    }


}
