import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Cezar {
    private static Map<String, String> symbolMapUp = new HashMap<>();

    public static String toCode(String text) {
        StringBuilder res = new StringBuilder();
        for (char el : text.toCharArray()) {
            if (el >= 1040 && el <= 1103) {
                if (el < 1072) {
                    res.append(symbolMapUp.get(String.valueOf(el)));
                } else {
                    res.append(symbolMapUp.get(String.valueOf(el).toUpperCase()).toLowerCase());
                }
            } else {
                res.append(el);
            }
        }
        return res.toString();
    }

    public static String deCode(String text) {
        StringBuilder res = new StringBuilder();
        for (char el : text.toCharArray()) {
            if (el >= 1040 && el <= 1103) {
                if (el < 1072) {
                    res.append(symbolMapUp.entrySet().stream().filter(it -> it.getValue().equals(String.valueOf(el)))
                            .findFirst().get().getKey());
                } else {
                    res.append(symbolMapUp.entrySet().stream().filter(it -> it.getValue()
                            .equals(String.valueOf(el).toUpperCase())).findFirst().get().getKey().toLowerCase());
                }
            } else {
                res.append(el);
            }
        }
        return res.toString();
    }

    public static void initMap(int shift, String secretKey, Map<String, String> map, List<Character> rus) {
        List<Character> shiftList = rus.subList(rus.size() - shift, rus.size());
        List<Character> secretList = new ArrayList<>();
        for (char el : secretKey.toCharArray()) {
            if (el >= 1040 && el <= 1103) {
                secretList.add(el);
            }
        }
        List<Character> otherList = new ArrayList<>();
        rus.forEach(it -> {
            if (shiftList.stream().noneMatch(el -> el.equals(it))) {
                if (secretList.stream().noneMatch(el -> el.equals(it))) {
                    otherList.add(it);
                }
            }
        });
        map.keySet().forEach(it -> {
            if (shiftList.isEmpty()) {
                for (Character secretItem : secretList) {
                    map.put(it, String.valueOf(secretItem));
                    secretList.remove(secretItem);
                    break;
                }
            }
            for (Character shiftItem : shiftList) {
                map.put(it, String.valueOf(shiftItem));
                shiftList.remove(shiftItem);
                break;
            }
        });
        map.keySet().forEach(it -> {
            if (map.get(it).isEmpty()) {
                for (Character otherItem : otherList) {
                    map.put(it, String.valueOf(otherItem));
                    otherList.remove(otherItem);
                    break;
                }
            }
        });
        symbolMapUp = map;
    }
}
