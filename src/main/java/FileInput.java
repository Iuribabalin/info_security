import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class FileInput {
    public static String getTextFile(String src) {
        StringBuilder res = new StringBuilder();
        try (Scanner scanner = new Scanner(new FileReader(src))) {
            while (scanner.hasNextLine()) {
                res.append(scanner.nextLine());
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        return res.toString();
    }
}
